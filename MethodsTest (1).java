public class MethodsTest
{
 public static void main(String[] args)
 {
  int x = 5;
  System.out.println(x);
  methodNoInputNoReturn();
  System.out.println(x);
  methodOneInputNoReturn(x+10);
  System.out.println(x);
  methodTwoInputNoReturn(3, 4.0);
  int z = methodNoInputReturnInt();
  System.out.println(z);
  double r = sumSquareRoot(9,5);
  System.out.println(r);
  String s1 = "java";
  String s2 = "programming";
  System.out.println(s1.length());
  SecondClass sc = new SecondClass();
  System.out.println(sc.addOne(50));
  System.out.println(sc.addTwo(50));
 }
 public static void methodNoInputNoReturn()
 {
  System.out.println("I'm in a method that takes no input and returns nothing");
  int x = 20;
  System.out.println(x);
 }
 public static void methodOneInputNoReturn(int a)
 {
	 System.out.println("Inside the method one input no return");
	 a = a - 5;
	 System.out.println(a);
 }
 public static void methodTwoInputNoReturn( int h, double k)
 {
	 System.out.println(h);
	 System.out.println(k);
 }
 public static int methodNoInputReturnInt()
 {
	 int c = 5;
	 
	 return c;
 }
 public static double sumSquareRoot(int m, int n)
 {
	 double k = Math.sqrt(m+n);
	 return k;
 }
 
}